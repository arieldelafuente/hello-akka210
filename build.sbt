organizationName := "arieldelafuente.com"

organization := "com.arieldelafuente"

name := "hello-akka210"

version := "0.1"

scalaVersion := "2.10.0"

EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Resource

EclipseKeys.projectFlavor := EclipseProjectFlavor.Scala

EclipseKeys.withSource := true

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
	"com.typesafe" %% "scalalogging-slf4j" % "1.0.0",
    "ch.qos.logback" % "logback-classic" % "1.0.9",
    "com.typesafe.akka" %% "akka-slf4j" % "2.1.0",
    "com.typesafe.akka" %% "akka-actor" % "2.1.0"
    )
