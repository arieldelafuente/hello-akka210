package com.arieldelafuente

import akka.actor._
import com.typesafe.scalalogging.slf4j._

object helloakka210 extends App with Logging {
  
  val system = ActorSystem("HelloSystem")
  val helloActor = system.actorOf(Props[HelloActor],"helloactor")
  
  logger.info("begin")
  helloActor ! "hello"
  
  helloActor ! "kamusta"
  
  system.shutdown
  logger.info("end")
  
}

class HelloActor extends Actor with ActorLogging {
    def receive = {
      case "hello" => {
        log.info("Hello back at you")
      }
      case _ => {
        log.warning("Say what?")
      }
    }
  }